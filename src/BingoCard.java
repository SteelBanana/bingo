
import java.util.ArrayList;
import java.util.Random;


public class BingoCard {

	private static ArrayList<ArrayList<String>> card=new ArrayList<>();
	Input input=new Input();
	Score score=new Score();

	//ビンゴカード作成
	public void bingoCardMaker(){
		System.out.println("今回のあなたのビンゴカードはこちらになります!!");
		for(int i=0; i<5;i++){
			ArrayList<String> cardLine=new ArrayList<String>();
			for(int j=0; j<5;j++){
				//free
				if(i==2 && j==2){
					cardLine.add("★★ ");
				}else{
					//数値生成
					int num=new Random().nextInt(10)+(i*10+1);
					//重複チェック
					if(cardLine.contains(String.valueOf(num))){
						j--;
					}else{
						cardLine.add(String.valueOf(num));
						score.addScore(num);
					}
				}
			}
			card.add(cardLine);
		}
	}

	//ビンゴカード表示
	public void bingoCardDisp() throws InterruptedException{
		System.out.println("<現在のビンゴカードの状態>");
		System.out.println("――――――――――――――――――――――――――");
		for(int i=0; i<card.size(); i++){
			ArrayList<String> cardLine=new ArrayList<String>();
			for(int j=0; j<card.size(); j++){
				cardLine=card.get(j);
				System.out.print("| ");
				System.out.printf("%-2s",cardLine.get(i));
				System.out.print(" ");
			}
			System.out.println("|");
			System.out.println("――――――――――――――――――――――――――");
		}
		Thread.sleep(500);
		System.out.println("push any key>>>");
		input.inputWait();

	}


	public ArrayList<ArrayList<String>> getCard() {
		return this.card;
	}

	public void setCard(ArrayList<ArrayList<String>> card) {
		this.card=card;
	}


}
