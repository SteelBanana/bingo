import java.util.ArrayList;

public class Judges {

	BingoCard bingocard=new BingoCard();
	Input input=new Input();
	private int count;//カウント
	private int reach;//リーチ数
	private int bingo;//ビンゴ数

	//リーチ・ビンゴジャッジ
	public boolean BingoJudge(){
		reach=0;
		ColumnReachJudge();
		LineReachJudge();
		CrossReachJudge();
		if(bingo!=0){
			System.out.println(bingo+"ビンゴ!!!");
			return true;
		}else{
			if(reach>0){
				System.out.println(reach+"リーチ中！");
			}
			return false;
		}
	}

	//縦リーチ・ビンゴジャッジ
	public void ColumnReachJudge(){
		for(int i=0; i<bingocard.getCard().size(); i++){
			count=0;
			ArrayList<String> searchArray=bingocard.getCard().get(i);
			for(int j=0 ;j<searchArray.size();j++){
				if(searchArray.get(j).equals("★★ ")){
					count++;
				}
			}
			if(count==5){
				bingo++;
			}else if(count==4){
				reach++;
			}
		}
	}

	//横リーチ・ビンゴジャッジ
	public void LineReachJudge(){
		for(int i=0; i<bingocard.getCard().size(); i++){
			count=0;
			ArrayList<String> searchArray=new ArrayList<String>();
			for(int j=0; j<bingocard.getCard().size();j++){
				searchArray.add(bingocard.getCard().get(j).get(i));
			}
			for(int j=0 ;j<searchArray.size();j++){
				if(searchArray.get(j).equals("★★ ")){
					count++;
				}
			}
			if(count==5){
				bingo++;
			}else if(count==4){
				reach++;
			}
		}
	}

	//斜めリーチ・ビンゴジャッジ
	public void CrossReachJudge(){
		ArrayList<String> searchArray=new ArrayList<String>();
		count=0;
		for(int i=0; i<bingocard.getCard().size();i++){
			searchArray.add(bingocard.getCard().get(i).get(i));
		}

		for(int j=0 ;j<searchArray.size();j++){
			if(searchArray.get(j).equals("★★ ")){
				count++;
			}
		}
		if(count==5){
			bingo++;
		}else if(count==4){
			reach++;
		}

		searchArray=new ArrayList<String>();
		count=0;
		for(int i=0; i<bingocard.getCard().size();i++){
			searchArray.add(bingocard.getCard().get(i).get(bingocard.getCard().size()-(i+1)));
		}
		for(int j=0 ;j<searchArray.size();j++){
			if(searchArray.get(j).equals("★★ ")){
				count++;
			}
		}
		if(count==5){
			bingo++;
		}else if(count==4){
			reach++;
		}
	}

	public int getBingo() {
		return bingo;
	}

}
