
import java.util.ArrayList;
import java.util.Random;

public class PlayBingo {
	//各インスタンス
	BingoCard bingo=new BingoCard();
	Input input=new Input();
	Score score=new Score();
	ArrayList<Integer> DuplicationChack=new ArrayList<Integer>();

	//ビンゴプレイメソッド。
	public void playingBingo() throws InterruptedException{
		int num;

		while(true){
			num=new Random().nextInt(50)+1;//数字排出
			//排出数字重複チェッカー
			if(DuplicationChack.contains(num)==false){
				DuplicationChack.add(num);
				break;
			}
		}

		//行指定
		int numLine=(num-1)/10;
		for(int i=0;i<3; i++){
			System.out.print(".");
				Thread.sleep(500);
		}

		System.out.println("でてきた数字は"+num+"です!!!");
		System.out.print("push any key>>>");
		Input input=new Input();//空入力
		ArrayList<String> searchArray=new ArrayList<String>();
		searchArray=bingo.getCard().get(numLine);//検索行配列取得
		//置き換え
		if(searchArray.contains(String.valueOf(num))){
			searchArray.set(searchArray.indexOf(String.valueOf(num)), "★★ ");
			score.subScore(num);//スコア
			System.out.println("おめでとうございます!!");
		}else{
			System.out.println("残念ですが"+num+"は無いようですね…");
		}
		Thread.sleep(500);
		System.out.print("push any key>>>");
		input.inputWait();//空入力
	}
}
