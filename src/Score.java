
public class Score {

	static private int score=0;

	public int getScore() {
		return score;
	}

	public int getScore(int bingo){
		return (this.getScore()*bingo);
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void addScore(int score){
		this.setScore(this.getScore()+score);
	}

	public void subScore(int score){
		this.setScore(this.getScore()-score);
	}
}
