
public class Main {

	public static void main(String[] args) throws InterruptedException {
		//インスタンス
		BingoCard bingo=new BingoCard();//カード生成
		PlayBingo play=new PlayBingo();//カードプレイ
		Judges judge=new Judges();//判定
		Score score=new Score();//スコア
		int count=0;//排出累計
		int bonus;//ボーナス

		bingo.bingoCardMaker();//カード作成
		while(judge.BingoJudge()==false){//ビンゴしてないとき
			bingo.bingoCardDisp();//カード表示
			System.out.println((count+1)+"回目の抽選を行います!");
			play.playingBingo();//プレイ
			count++;//カウントインクリメント
		}
		//ビンゴした後
		bingo.bingoCardDisp();//カード表示
		System.out.println("ビンゴまでに"+count+"回かかりましたね！");
		if(count!=4){//最低排出数以外でビンゴしたとき
			bonus=score.getScore(judge.getBingo());
		}else{//最低排出数でビンゴしたとき
			bonus=score.getScore(5);
		}
		System.out.println("あなたのスコア:"+bonus+"pt");
	}


}
